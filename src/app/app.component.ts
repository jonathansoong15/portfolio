import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  imagesLink = {
    storybook: {
      imageUrl:
        'https://user-images.githubusercontent.com/321738/63501763-88dbf600-c4cc-11e9-96cd-94adadc2fd72.png',
      linkUrl: 'https://storybook.js.org/',
    },
    confluence: {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Atlassian_Confluence_2017_logo.svg/320px-Atlassian_Confluence_2017_logo.svg.png',
      linkUrl: 'https://www.atlassian.com/software/confluence',
    },
    vscode: {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg',
      linkUrl: 'https://code.visualstudio.com/',
    },
    angular: {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/c/cf/Angular_full_color_logo.svg',
      linkUrl: 'https://angular.io/ ',
    },
    nx: {
      imageUrl:
        'https://ultimatecourses.com/assets/category/nx-6c132a35ad2a671dd3cf042dbbc4e1d941a3c52971b01c9e332b4300dba07df4.svg',
      linkUrl: 'https://nx.dev/',
    },
    akita: {
      imageUrl: 'https://miro.medium.com/max/700/1*C3aznPxrQywClya3RTWKSg.png',
      linkUrl: 'https://github.com/datorama/akita',
    },
    jira: {
      imageUrl:
        'https://www.atlassian.com/dam/jcr:75ba14ba-5e19-46c7-98ef-473289b982a7/Jira%20Software-blue.svg',
      linkUrl: 'https://www.atlassian.com/software/jira',
    },
    kubernetes: {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Kubernetes_logo.svg/2560px-Kubernetes_logo.svg.png',
      linkUrl: 'https://kubernetes.io/',
    },
    docker: {
      imageUrl:
        'https://www.docker.com/sites/default/files/d8/2019-07/horizontal-logo-monochromatic-white.png',
      linkUrl: 'https://www.docker.com/',
    },
    karmaJasmine: {
      imageUrl:
        'https://repository-images.githubusercontent.com/201661126/10f63d00-c8cb-11e9-9c36-87b4ea563add',
      linkUrl: 'https://angular.io/guide/testing',
    },
    protractor: {
      imageUrl: 'https://miro.medium.com/max/1838/0*fpvofUviQKA6TRQ1',
      linkUrl: 'https://protractor.angular.io/',
    },
    gcp: {
      imageUrl:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYpiQmS7UwSJ-urj0SoZ83M-k4ms5oJKgvxQ&usqp=CAU',
      linkUrl: 'https://cloud.google.com/',
    },
    awsEks: {
      imageUrl:
        'https://www.alfresco.com/sites/www.alfresco.com/files/2018/Nov/amazoneks_twitter.jpg?_buster=CFwpRD-B',
      linkUrl: 'https://aws.amazon.com/eks/',
    },
    spring: {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/800px-Spring_Framework_Logo_2018.svg.png',
      linkUrl: 'https://spring.io/',
    },
    kafka: {
      imageUrl:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUgAAACaCAMAAAD8SyGRAAAAkFBMVEX///8jHyAAAAD6+vofGhwGAAAgHB0cFhgVDxE6NzjW1tbMzMwXEhMaFRYQCQt/fn5EQULy8vKWlZUMAAXj4uPS0tLe3t6ko6O3trbt7e2GhYXv7++0s7Osq6tzcXGbmppcWlopJSaQj4/CwcFqaGlLSElRTk8oJCViYGB5d3g2MzQxLS59e3w/PDxdWltsa2smwUzCAAARfklEQVR4nO1d6WKqvBatgUSqgCKK8zzUatX3f7srItk7E6Cn1s8r69c5BSEskj1n8/FR4qEY1ur1eqf/7GG8NoLR6SskMcLqqRc8eziviv4PJS6rJGAuod/tZw/pJbEI/ZTFK5xwYT17VC+H/ppINMbTMpzXnj2wF0PkuQqNMWzae/bQXgp1Ymt5PE9KMn324F4IkSwdMUg5J4si8EzzMZmTpZwsiJWTweNZTu6ePcAXwZIItPmUEN/Df6KtZw/xNVBBAtImh8Wo05muCNLizB0+e4yvgB6akM5hef1r+4T+HJZTsgBmsIz9Lfr7lMKUnD9tdK+Dfsj5cjfCkQEwSepPGt0LYcSXMPuS4j1rPlf9xXMG90oYcNuHjqVDHc5xdf2Usb0UNtwYJ+qxdEqyyhNG9mKopsaPt1KOtXyuzsvQZB748tXIwR5XN2H0hKG9FoBINcxT5wdp6W/nIWtGLksii4NnadyJcmzKZaRfLu08NFOtzULl2JZr7WqZUsxDg9uRSgS3zVd2GUnLx5hrZvtTOvTNOdYs+xISYNpVfJEucB7LdEMRbKrAF1bcyxDilKQUkfmAtX0mbJUW/QwHKM/tfBt+a0W96WD13Rp3ykqCjw+cQnTJ9zKKot6P71cQv3rjJ1pUCPEd14mzE5vu2xdejYWcjUNISImD87OOVtVEJ4JfgUfdwbtnJDZCqksB8zUS0loQuTaD+eTNUxJtTdkPXtgald0+UO2pm/eelPUsJnU1K3VS1Z/sOu/tS47MTJIf9fSObyzNqLL3jlz2DEwyMlBPjrImsLd/b0uoXtXVrXhEzuOcYe0N6zqB/+buZHAisvK2yUYn8X58LYEcZKn50TuhfsTmI3PIp9bDjgSz0yPkbI1TPEftspwgGuzTBe6xgaEoAFVmnGmcLdtBEI3XuFK1DHGc0U3WrSaleEUNTUh65Cp6uQd+7cPfjPU/jVFiaJsDkAOQkATHMvo7YLIsTS1A5Bdfw444a4cVfqSscMknEsWBiWQwQs7Rbj58nP955BEJ1ZS+EqE42CaO3xB5RC64iCSKLzjmFYL/HSHZj2qdp0Sd84g8pcEz+6AMDyzMbJu8zfHYJ6wvNvtLSXwg3/eht02QRyS3Ir2tcmwIFYJZlqRVISkeOHGDhUf8KmOQlLf4bckfBPvyiOSpMo2lOeSVbXSUcQvrM5WlDySyXuG5OyCSL5g/yOXlEcmLLzS1p30oFcqckX9A5BgFqJ5DZCvHs+FFA0qhNC5ey5SRf0CksHXoz4m06tM14b42mXV1jwl1VSpZUEWtanR8n4cTObRx4PmPibS6n0QIfHshaY4VtQqzrrqRrwBl/X7mnR5OZAt2alSY59O/JLJXoWr6wKZV2eq2kGcjTckGn6wajY4v8XAiXRCQdH4cXPsgWDxf9zgi20fDhm1GDx3hzP6cD9KuCAsYlQrRbtbNHk5kB71stKb+YEb2qL5/wIUuoSBoinckVxkiGelJ5mdavA8nsgseFl40jyeym5PXPqUnLvdUOJORnytltS3Sk64xmJk80KOJnKTTojoT7vtoIgdC7kCD8HhZH+2Zuv4d97gYjRtrYUrn8PNwIrnTEAoi5tFENvJ4rFT8s19tDZTkWIxYKVJHSCs62RPy8USu+fWFbMmDiZzm8xhvm+3tw/zTLrCrOWVpDyey+Qwi60V4jI1KJBxZVguM/GzsvUQGtV53Op2OO3k0/DKRQWd0vm+3V8v61bAi6xnmuY5bzdI+jts0K/lKfkWamUiIc7XlhHq7tfEJDX3fp4RuWtfwTb+WIlF6VvzP6JCO3h8nB6/3NREZ1BCk+9YWG3q5b0hJOBunATl+err6JlKy3yG72WTwvdlTUwcbj0yG1trYToTkp2uMRJ6Ic4VPREu0syG+BwOqhv7k8khdQhNc83B9Ev8BlRpfjpFsz8b6TK9yPlW0mpdNwdvz6LXJWYPf9/q40sJ2SSO9UDS1dQUVjKzj5WJt9BLBJpmmeDaRyHoQK+D6W0XPMd+OA0zcYnQayZm6pcS8bCKP/EEZFcRStFGK7hjdxwzxqEKab1kLJ5IVNqSDhmrt+F5aC7TQFfb58yKdBgxEdhGPQuXWyNFJkkt5VzEis4MWE9QoQYj+TbVmih2/ZJlIcUIqBVNLSat4ZAAOV2ctUcl80iiUOtATiXqUUKGS8NvkLpyZ/AUi8TrABFhbkxo+MykTeRLMaDWqLdbvVWeiBliuz4LLvo7VIWRQMBWiJRLVuoZCyGOlLQ1Oft4b/zOReB1gNWltzKViZLkQiRxiznUFfGKAVBLE8bin208/zoF8bUyVQgWJrEHsyZ/hk7dmHs+nNtKZcC+R6AEFNZnFY4XtJ+miT4hE8RqTO9JCz+Foanc/rH67U2vflEzSEImqNJwmlg8LZX0JTMGvrkQ63hnoePxfzzcS2QENL9TffKwU9wPfl/H/JESuYGUzV++OWDsQk8pexTuhEhlA3ZArZHnFbmNn84M49tmcdFSfPyFyODvOZscvPuLN+b+z49EyEIl2IYQndFtpxwxz4/0yLhE3zSAibWRwNQxPjWct/Z18sEokmKXVvfBCPexP+WQ1qrX77Wg0mMsGhTB+8GwEYaQQOYTSY18I9/ddfF9KJ6Movu+y4VKJyguRbbx3zuisIcP8l+oeFSJXXB7ZnvCuUDOsiue00Fzt7UQZZiAy00W0wEFz1oK90UAXd+0uig2PmeiKXIhExk9Gz66JK/7q3yETCeEnW9xcEiDB5K+l5SAG/+4hcgapkU+Bxz5ehUfJnZwI971QMoL3nbEfuwtvx9HscLgDEpEtyJlJdgHaburM1IHhJ7qDyG9+dU+UJ7gENFSJEd7ghUiIyEsRUAHIUP6l7e8ikWPEoxQ3AkVXbWpM/RY2Om4mEuwB+0uc7BboDlfOlMZYII1+IRLaI6ndvACo1Dk7OVgYApHIEJc9AlT1r8+Toz2UNxOJ35+kH0DkGWyZA7h0MpGhmcjOI4mMkO+kxDugFoHqO0+3kdS5iUhqwTpjoexn/Ji7xSVAb1he2hlq5JFLm/bmUJuqDGEGndkMIVV45NuI9JbIEFfi0DxTYVTB4ForysbUIuCxyqYC0kjdqmfxDLrxvrBabiOyAvapatIFWb25EsDiV8wfczuaLWp4+ju93RGRHFR9k22oJDL68dzVuJFIDk2EAZSCoXECfscXIiNk7lLj7mB09183yGGGaDx9mG7minTetOhOInUijXfWy+j1OBODFsgBM0423Mr4111EIFKjxrifbe+Nl+L66E4idffl1muGbuWV9AmRR1i1rGoI4BweGbQAz0WTMgMidcZcAv7QNxJpNhVw1F0X7breNz0nIRI3qtE3ABG8Xf2Fh+12u39TehOIZECpKgf5WvBmxkv17iPy6xOYVIIMRYjkejohEvuU+sBuRzhDCexG022TEUK8/WZSfG8xMn+WfB+ZPZcFIcxIc4NfOdWQIJdIF55LlRv8mhmmTEtc2nhtaxP7bSEAVz1KqYZdnGqIz2BxqiFcFAzvIoO8DQFHR5ZI/GEzhP7iPmVDLXBslH36fLZlrISVqGwETXJ2leSkTT0r+VVvSjFB5js3J7+ij28opZbEFdq1Y3xDPAt6I5HBxxaMK+mx+btlnvER+BOkal/Un+SEXctgoKbvIB070aRj2c3p2OjD2vHrSOKqzwP6xiKY4E6D/ExkwOOsTHLkYbelKsuuiLhPmBIphfIdOuAFAi0vo0AgWOtLqm4tEIiEZudiVBDesnsyXAm05c1Eokf3xC2oQTXXo4IoBTdEt1LtiUN2q8ViMfk0lqxUyWQYNH+pZCUWui2TuOIpJeYY6ts4Y3cQicLgVCQMfHymF1XWXkp+fcTrRzaOmef7vptVb5ZTRJXLpETkx4ZfTRRXEAswGCJIxN9BJJJrouyAfauGr6agkDK4RkvV3NfhQWV9FyLb8FVLoW88ss600sr6MiTvChIJTnVaHZQAhA3Ttl8fIlsG+ZiFCk2dbe8rp0kNB7uh0DQZJ8wsT+iGAYFbVtFcE4ule4hEj+7O8Imwcu2duritJjIasbOeW0J+vs9ZDQQ/2n5ocelzKOaZHZNuMBGJ0oiCuEJL19spztMJ67u7iMRCBccapnBlZ6MwOcMzSoh65DLpNC83jjaqPRS6x8W4+90UCreKF+NfibT2WnGFoxteVZQY7aawQu4jsg8aFQuPAClady5KlWgn6FkxfDTN3h5C+VsZSSaRTVrXJVcXtodkT0mVSBQatW20iHHBg01O8H7aA/HzbncSiea8vUdTfoEiDDb5AUHZXoSifpDicEvP/EE/G8eurQYuNfDmSDeghn9Gc8VIJBq6kLg7YLpcuh+M6lG9tzgoxtmdRCLHykchUbRC4mvT3aJXjzq9wUH5TrEc0OxvDQ0hGZ2LSyqCEBQT08G4wLHoFjp41/A1JxxRi0Jh4MyhhBCqabp4L5EW1GHhqI1UyWwn91VLjjSR4XpTU59q+35LFrbIJpH8wcHtmzrRokHpKHTdcab8hqqwe4k02TpqFZzASwaR5xm1pRRzyc5eziJjc7vSQuCObcZo6CjR7KGXl7Wdin217oz+oMeCwkUPF8JtM6w9e83rMvXp1/5oRtLfe4QNdG1e0MZ3JYPTuHnjO7Z3wSwUajVPZibJUq7YvYPIDzALQ+xAGaIJlTjeEC3kYnwVaZO5rSHo/c0dYDXZfHsrBkxkMNcnADRBqOtZ43sDuwKRKOgqzI2J4Q3GlTXKrgYVec1BeCxYE7ce8pdYtMuK4IHhMmR8oEd0ARQ7Vg6/QaQgVLD67BJ9k9eeZnuIijwieRmCJoMacOc3o5YoJrLKEkiJ4wG9/p1VhUa9/R9frpVlZBcbyt30F75ApJdeXyIyvby082vlpwdcIaLWnihU2uQQS61B+gvzx2CL9/3JbKCUSeTOrSaQiLTWTjU9It4/+nGIk5ZlMNsh86QzQJdczxfEWzO9jEwkv7pU87h3+RExdtWZEOLCff20yeuApuffTSQvvLA/FVWEYsv/XEogX9xaNpqVMN5BEbLdDVso/vW+Qe/7wGIr8qyGDwtTyFyDPCKRZlZCTKgFwkOeNIg6Z0R/3nx/mNz3tr3JeURCtFWVs1AYWn7/JpdIyO3JIhs34vylmoxXRm5rWHDLXFHdBCiT8Tv1fy+N/GbFYBEItXhos8V/qA/n83BVGGYicRULnXFXsH4AHssWu1GLx3+95tRQMok+tnY281fLftnQXUL9iPsO2CHZaj1mMVbnng2s8hMDGMFKCUh6RN5udcHKHE5PJuTjzOUXwNLReui61qRqZxYBobms8A1g+sQS0xXzZH7Zyn3rDzVkfapKYxNmnO7t3vnzaR15G7LApGZO9gztJitO5Z0/+TU0VZ5dmdQoj2ivL/qbvfW3BXLUsF3RsBNMNE2NilRH/h9DLEjzQkKlT56GWte5vhHO8mj13T95OkfyziPbbieKeg0HLV3m6ysnlj82IaHjun5IyHH85jSKe7vWqV8oVJAbP/RjRb3u4DRZjJdlABJ/DE0wdXqwcE01wCUQ+mj7u9izBjnVb/+pwwKAxj5VOfoF+Zlf2qn9fw2UzZKnHWQFyw/05YNvnmKOcmyVt1GiBIBvz9EExaFiynlnz68YYLuQauOAutHukyiBIfa5EAH5GVqms/IA+yLVGbksiSwOHu7WVEW1ShlZHJAUJMqx3O2NJQCQ8Ff2ItTKGpQbAJ4Nk9tKwF5AY8PTEhyor6l7FI60Sl/7JqDmgf4RzUnUqYb5pYjMB45HOvM0j93BX2Moq8sKYY4SBozsB6NOb7DGGRkWlsZPEYg5m8t+P3HXHc3vU1Eixikni7gvJWQxBJ4h3X9d7aV7WBRRVoWA5qsiJUzIKIsq0MinBKDm674mZKhGK5GB9lo3KR2/dGluRstXGkWQSU4LnxI69AcMVfPYlExKdX0nrGVjftk4SWi47paz8Z8wrNXPqJU2eIk/xv8AjjAMDGvKzyIAAAAASUVORK5CYII=',
      linkUrl: 'https://kafka.apache.org/',
    },
    figma: {
      imageUrl:
        'https://carefree-creative.com/wp-content/uploads/2021/01/figma-logo.png',
      linkUrl: 'https://www.figma.com/',
    },
    zeplin: {
      imageUrl: 'https://cdn.zeplin.io/assets/lp/img/icZeplin.svg',
      linkUrl: 'https://zeplin.io/',
    },
    git: {
      imageUrl: 'https://icon-library.com/images/git-icon/git-icon-18.jpg',
      linkUrl: 'https://git-scm.com/',
    },
    appSync: {
      imageUrl: 'https://www.qualityology.com/2021/aws_appsync.jpeg',
      linkUrl: 'https://aws.amazon.com/appsync/',
    },
    awsS3: {
      imageUrl: 'https://miro.medium.com/max/580/0*PQM2oxNUUceATC30',
      linkUrl: 'https://aws.amazon.com/s3/',
    },
    awsLambda: {
      imageUrl:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYUAAACBCAMAAAAYG1bYAAABC1BMVEX////1hTSdUSX1hTa3YCn6iDWgUyX1gzLpfjKYTiTVci7s3dWWQgD1gCr0ehL1gzHiik7//Pn1fyX2jUPMbSysYjaaShj+8Oi/lID5waT6v5z2k1P0fR/5s4f3o3JrOxn95db8v5L3mFy6inL2kUv96N/5upT+9vD72cP4qnj7zrP+8en1ijr71b77zLD5t4783833omm0fmJgJwD8vIzQsaKhThWhWC7Lp5bncxby6eWZRgS4dU/k1MzmsZHVbyP0dABZMRaKSx+lWiS8azBnMwlzPxvArqOYfGx3Sy6zn5OObltpNABoNhDYzcWGYUpdIADegD/KvLPno3rGe02pZkH6rnaSNQCrclYcXhlIAAAOUUlEQVR4nO2d/X/aNh7HEW7itInQLHc4gMxcDzAPhuA+bZc0t6679bbddrvb7nb9//+Sk76SbAMBm9SG7FV9fooNlmy9pe+DJJNGw+jPKufYN2DUcF69dufHvolPXM6r05vTEzYwHI4n59VXN83m6QkirDc3hukoioABUECGw3GkGSgKhsMRdP21ZpBSAA5Tw+Fg4gzazeYGBcPhkHrVfN1s3klBcBhMj31/n4a+bDe3UjAcDqXdFDgH6hoOtauIguFwCBVTEBxiw6FWlaEg/IPhULGul7mDchQMh4p1fXnzlxN/rDOBshQkB5M/VCLOoN08I5jhiZyiKE9BcHAXhsNHCxg0m2eiRQWIabQXBcOhAikGkgKXzUH03zT3oWA4fKy+vtDd/ixrVPxoTwqGw8cpsz05Cmh/CjCvYTjcU9VRMBzuryopwLyG4VBet39devBHtRQMhz10/rz9DUMDfz6snAJw6BgOhTp//rr9+AkiIjlwP29WTQHm+QyH3Yqei/yAUwCdPKqBguBAOsd+0Aet8wvRrjVT4BzeHvtBH7QORAG1jv2gD1qGwkNQFRSePEUnRWAMhV2qgMLJo2+/aD49QztBGAq7VAmFL4TePTqztg8JQ2GXqqPA9f67J2dbQBgKu1QFhb+9ePH9L5LDt+8f322bDIVdqoAC7lz//YcfX3z/vR4S7x492XDXhsIuVUCBLng5zvlPP7xPhwS3TU9XbZOhsEtVUNCzE84/f/rHzy9+ViTev8uDMBR2qUoKQpzEry9SEmCbDIVCVU1ByPntpx//9eIXNSL+TQyFItVBQej6t1/fvXssiv7MUChUXRQajUu5YGQolJCh8BBUVaTKbdDtq69nuZINhfKqImv7/dXlh68uLm5uvgujrGRDobyqmMF42m7LFv887mclGwrlVQmFx+qDz102Sks2FMqrYgqEpa+RHJjCn3qXR8UUEMGBKnkvCjNPKHPu0Z3H0ZbPnWA0CcNkMhp7dxe/XlwlcoSKv1am6qopIBKrGveh4BFQnJ6YM3FM0+Mryg9xkh77WHx8JVuj02MYrseU9PNvgqXqiMtJr9LxsuiFXFuw5xSXqLpyCgirGveh0KWWEB3rEx6y+TEb6uOQ8EPE9KHTE8cMfqRpOKC2lYow/47yO6L8iil0oWMMC78X28VVV08B0QmUvAcFpyfbkWQhFjQ71U7GQ/A5C9TxjPITiAoLNSTEygvRcLOCDuaf2GG1FHiZqAwFfqv24SkgGSjtQSGgSPXk1PJfiXYjE3U0lmMF6yx9Dn1bNLcTAwRCGWNU8qCbo+FTpIDAVOxBwceqI6edv7EU7Yy0o7iSXyBJ/hh39V98+C2jKJhPABZiG03zSVKwRTuUpxC52rCT1JpE4uYtJj29E8perh2DPBb2yYF2xwt11RiLq/DGYPgkKfB4NdqDwlw2pbXyVNIxSHc90xZLOYYI3ELsqCFjD1aaxrLjxpo+TQoID/agkECDQwPiNPeGhsMyFpWYMscAbgKcRuYgpIaMCkWrFeymMJt3fN8fLcbpVePOYrEQu/2DUc91++o9+nkSuz0//fHMjEKw8EfTtZA1mo98fyE+XaMQjRe8tqvFPJ9C1ESBW+rSFDxhRnhLQku5+mwgzqpeDn7Dztp7JI5hKncu3Uf6jM4UtN7cOyhM3RbFQpS2+oE85wuWrciZtLBt24QSfj6IGeEHmJFxnoLX6BLGC6CtMDcsHF+cEydnqxTGvRalqrYwS23qooCwOltMoavadAJDQt+aI5wFwuLuIZAlfZQ6Bhg8TPQ+sEgcToFh2EohChn4JIRkstGVFKCBZwMdNNjMWzI7PZhnFEgQpvFdKw0thi5OTwZujsKEkVxtrdR/1UaBl/W4FAVoY8SixpKtpAz9FAq4BQqPA44hwumDOVg1X9gNdpjebRScUHojzNNuG9z/XFOwrJDYmGJbYraEsyNQmR1HmoJl9TCyiTyPWqoHzWT0zM/bCMU5CjKGI2LowDcU9Dop8MtKrTvLxhcxKLSIpe3llGpHIP6ye+CvwQyNWRYIqazbIpihfme4hcQ2CgsGEK6CIJgOSGryJAUbu1eL0UC2qEXiydXEggM6zyhwG9VLEtn3EZEV9CVaN+n3ODjxiaQwhmGDk3kQLBMZRjj1U7BOy1CYgEGa6oenOuocgrtI1Hne6OKx4ViaMOUmQ202LPGzHXH/zp943UYhyWXokWgv241SCtgX358N5GjoRemB7ACSAnHFAIhGgIFC315KtCNx9VDG2JICjG6diULVLa9+CujstJhCZAl7j8QzBmwl4AHHYHGzIR6dwwEfgPUDEDVmnITlppHEq+53/AT4FgpRzN0wQ+rIh2I9TUHHvwuZF8rqxunAlRQQUh5pJPu/+FP6N21sxDSSotATfvmtugCGup6SqZWCcA1FFKThkeZFhBNZCC59ZNDwmPTFwIsG3BdYuTSalzBg+akkO7fSVEDBmQvpeGCySkHn8cN833Cygrpp/g4fwChhS/2Vga4Lnk9SWHS4uvnzh6Bw8uRx8ViQ8Y5sidFKygBZAW+LBVVPJUY3T5NhyOBcUzvzPmE4I5E6vSIKeUVTMCo5CsscBV2dA752EDU2sja4Rrix4ertOcyyNrM2ZympHYDCWbPYLwwpWGN5k9Dr0240A8cwgecDa6ocQyffRroNl6OExx4yPkQ02IeCF8y7fujKiDNHIchTUGAhoMsoZOmNzFzEmIY/sjl6MKz5rC0Yd/xkIGOvA1AQXqGQAnR/0l9KxVa+gUV34c/p2spnLzE4CjF6kLueHous1CfSVePJ2kfbKQxHPUvkUTwjs+5BoZcWFOhgYgGmJktgxO1qCtEidLGsDR2GAvmuRL7gyIk8wkAy6kzns4GQNU6fyiHgKMBwyLTCGwrlFkJH0KNzXbSAQhdj3h48uqJs4O5PITd3spWCj1MKY4uKxhC1uQN0CAr4P2VyZ4jq1oSQCn/kVF2YPjc4BgJOVE0oTcRK50rPh/ZG8dpI2UahK1eLaNKdDuX84UeMBStHgWaBWjYWlmCGKAtHi0DWUTcF1ik1jzTBmxSylIFBx86GB4QlMnGSfc3HvGjbzre5vQcFaGBEdOaxPwURSCut+oVc8DDQfsGBBLClfiPnEBTopNScaiQNCE0lMyEdhYY67lFdf6nnVrWdhXAvo6YplLVIo3xLNGDZdU8K6Y6TRjpEvZaVN1VOS98vjPt0HWtCaqeAe+VWeRZyYnqcCk4g1dNl41mZmXX0JJk2Qp48gbNp5Tne4Z3Xq5/gXPjiMLQ3hWxByZMTQyKwcHXmAJIrHqIS6DK6HeRSe60UiJhQKUNhdY1fSE4RqPEcKK+h3EI2OFKzm6gZHx0YAoT1MFZ7C7fbycuRjlMHxnJc7UkBqZDUSaShFH+P5BS9p65GqxSc/KPVSYFAXy5BAR7QQvn1ju5K5qlmjdOJVj04kO78Mxl3IzrwO4tFtwcFko1OL69DOCdqRXLRWnWCYXyPfEGU2Z05zrIn6cO3PLhrEk8jx+uoadjUIqkONuuRuikw6B8lKKhkYRMMU317Ivt+Ok+gHEOumZdqM5ItFk7UNLS1sRMuNW2ZEM8UZYRG+4t5Z8Ls+1BANsIsJkxNZcvOM5KbEChrMWzZ6TxSBFNKNOzMOz4hdWdtagKhBAWY9aWrs2+9XEykrETaIMp264VQUBDnN4UJVx9ubkfcQkFP+PNEitgYIni6n3eOhY1UzgrpvtPo07QaMkjS3HkhPY9I23htdu7Zq6egNoWVoDCGbZBrUaXczIjlyVkLdj5miXIPtkKyvN2PRjFVXcsmmLl3vVnUYWRd2BLBY5/BAgA3otZ0zrihgpUavyVWPDWFt+K0jjwHwpq52QxGhLSRJFmo5qg1NT5CY8/nd4eliR1RuE+etuGroaiNdWqigHU8WEzhSmz0DNe2rXiw/bOnY/i1r3Tg03AVXDSfhC7QCv3x5swG17S/qQnc5yJxMSVu0o0a3sTnEl4igLlWVVLUFdLcR+I7sHQAOyQtfhWRC8lJvmvMEyQmzQd+1JgmvDJfNsqy73LrFYdXQcO5EiXVRIG42h4UU3Du3PW8enb9K9s2SsOu7Og+W12ilc3g+13nCfvlTTmaTrD28SwYj+9Y+ou8oTfbOF31+wskvRvzFkl5Vf0uT+ZqDYXyqoLCNxc37Xb7lFNAufUVQ6G8KnnfWbxk+/y/ry9e5z2toVBe1b3vzD3Ped7tGArlZd79fwiqkwK8BW0olFBdFIbjkf/772/efGUolFAdFLzx4uXLl89ALxM5w2co7FLVFGbLxTNN4JmfuJggQ6FQFVJwouU0I/DyjzDGhOgvGQq7VFWkGgXT7ssMQTJAWOwbT2Uo7FIlY8Gb5wn8kYgJTrQqQ2GXKqBAkmeaAEfQizcRGAoFqoaCdsYDa9UOGQrlVBmFP0IX3TUIDIUSqoTCVjtkKJRTFf+LJLbIFjtkKJST+b88D0GGwkPQ+f9OD0EBvz32gz5s3f73pq0oEELroYBZEhz7OR+6bj/ctJ8QTCkKu29qoICpYVBGtx+euElnGVX/n4U5A2wYlJXev1Q1BYz176wYlVe1FAyD+6lKCphOgmM/z59TzyujYBjcX7cfLtprFAhm+1PAzDD4GN0+lxzONILYH2d2qhwFzqD4V/2MdkpyOBPvp7Xcq6WT9xZlKBgG1eicczijrZ7+FbR9KBgG1en8+Wed7CXi8hRwyzCoS2UpGAZ1qhwFbouK/xuB0b1VhoJhULeKKfAczdiimlVAwcbYMKhfX97soGAYHEjXX7bbWygYBgfUeZ5DRsEwOLDOL1MOmgJn4BsGB9a15iApGAZHkuIAFDgDkx8cSdeXN21BAVPD4Ji6vrxoU2YYHFvXl37F/5fUyMjIiOv/U9HgboJgJhoAAAAASUVORK5CYII=',
      linkUrl: 'https://aws.amazon.com/lambda/',
    },
    postman: {
      imageUrl:
        'https://symbiotics.co.za/wp-content/uploads/2017/10/postman-logo.png',
      linkUrl: 'https://www.postman.com/',
    },
    azureDevops: {
      imageUrl:
        'https://lh6.googleusercontent.com/i1-vL8GyQVJGCjcyaYINLkTIIqb9KVRdy4yqBJnaBaXtMyaZk5yPAl-i9D4H0IQBDvu0q6YOq9dskTn5wttyoRHzZwvrifFg0DVDXcbi2cbR0HVT1jjIpy77qypYn_74qP9JtuFU',
      linkUrl: 'https://azure.microsoft.com/en-us/services/devops/',
    },
    iis: {
      imageUrl:
        'https://blogtechdox.files.wordpress.com/2017/03/microsoft-logo.png',
      linkUrl: 'https://www.iis.net/',
    },
    nginx: {
      imageUrl:
        'https://www.nginx.com/wp-content/uploads/2018/08/NGINX-logo-rgb-large.png',
      linkUrl: 'https://www.nginx.com/',
    },
    appium: {
      imageUrl: 'https://logodix.com/logo/2116308.png',
      linkUrl: 'https://appium.io/',
    },
  };

  data = [
    {
      title: 'Web Squad Lead',
      project: {
        name: 'Corporate Banking Web Application, Thailand Bank',
        headcount: '450~',
        timeline: '12 Months',
        numberOfUsers: '3m~',
        numberOfFeatureModules: '50',
      },
      description: [
        'Led a web development team of 6 to deliver web application using Angular 10',
        'Conducted design sessions with business analysts to collect and clarify information written on user story',
        'Conducted technical design session amongst development team to discuss on application architecture',
        'Reviewed technical specifications and application codes created by development team',
        'Assess and enhance processes throughout the SDLC cycle',
        'Assigned tasks to team members, staffed projects and updated all involved parties to enhance optimal business flow',
      ],
      skills: [],
      technologies: [],
      technologiesImages: [
        this.imagesLink.confluence,
        this.imagesLink.jira,
        this.imagesLink.vscode,
        this.imagesLink.storybook,
        this.imagesLink.figma,
        this.imagesLink.nx,
        this.imagesLink.akita,
        this.imagesLink.gcp,
        this.imagesLink.angular,
      ],
    },
    {
      title: 'Angular Development Lead',
      project: {
        name: 'Back Office Application, Thailand Bank',
        headcount: '50~',
        timeline: '8 Months',
        numberOfUsers: '500~',
        numberOfFeatureModules: '5',
      },
      description: [
        'Coached and guided a team 9 in terms of software development, not limited to development, unit testing and DevOps related knowledge',
        'Setup dockerfile to containerize application, to deploy to kubernetes',
        'Setup development workflow and processes including integration testing, merge request, defects handling process',
        'Setup coding foundation, best practices and guidelines',
        'Managed to deliver application on time with 80% unit test coverage and minimal bugs received',
        'Acted the role of release manager to ensure smooth transition in handling application over for next testing stage',
      ],
      skills: ['JavaScript', 'HTML', 'CSS', 'TypeScript', 'Unit Testing'],
      technologies: [],
      technologiesImages: [
        this.imagesLink.vscode,
        this.imagesLink.kubernetes,
        this.imagesLink.docker,
        this.imagesLink.angular,
        this.imagesLink.karmaJasmine,
        this.imagesLink.protractor,
        this.imagesLink.nginx,
      ],
    },
    {
      title: 'Software Developer',
      project: {
        name: 'Microservices POC, Malaysian Electricity Company',
        headcount: '6',
        timeline: '1 Month',
        numberOfUsers: '3.3m~',
        numberOfFeatureModules: '1',
      },
      description: [
        'Designed and developed event-driven microservices using Kafka and Spring Boot application',
        'System managed to perform 1 million notification sends to FCM in 45 minutes without failure and able to handle 3000 TPS for db data retrieval.',
        'Containerized application and deployed to AWS EKS',
      ],
      skills: [],
      technologiesImages: [
        this.imagesLink.spring,
        this.imagesLink.kubernetes,
        this.imagesLink.awsEks,
        this.imagesLink.kafka,
      ],
    },
    {
      title: 'DevOps & Test Automation Engineer',
      project: {
        name: 'SprintX Transformation Project, Malaysian Utilities Company',
        headcount: '30~',
        timeline: '4 Months',
        numberOfUsers: '3.3m~',
        numberOfFeatureModules: '-',
      },
      description: [
        'Created CI/CD pipeline for WebService, iOS & Android application (TFS Build & Release)',
        'Enabled DevOps practices in the project',
        'Setup Test Automation Framework including code branching strategy and application architecture (TestNG, Appium, Maven, Extent Report, Spring Framework) for both iOS and Android',
      ],
      skills: [],
      technologiesImages: [
        this.imagesLink.azureDevops,
        this.imagesLink.iis,
        this.imagesLink.appium,
      ],
    },
    {
      title: 'Angular Web Developer',
      project: {
        name: 'Retail 3.0 Transformation Project - Digital Queue Management System, Malaysian Telco',
        headcount: '10~',
        timeline: '2.5 Months',
        numberOfUsers: '200~ average per day',
        numberOfFeatureModules: '1',
      },
      description: [
        'Liaised with client to acquire & analyze system requirements to be developed',
        'Translated design spec in Zepllin into HTML & CSS',
        'Developed Digital Queue Management System using Angular with TypeScript',
        'Perform system integration between Angular & Serverless APIs',
        'Utilized Technologies & Tools like QR Code, Angular 6, Git, SourceTree, JIRA, Postmen, Zeplin, AWS Serverless Tech Stack like AWS S3, AppSync, Lambda & DynamoDB',
      ],
      skills: [],
      technologiesImages: [
        this.imagesLink.git,
        this.imagesLink.postman,
        this.imagesLink.zeplin,
        this.imagesLink.awsLambda,
        this.imagesLink.awsS3,
        this.imagesLink.angular,
        this.imagesLink.appSync,
        this.imagesLink.jira,
      ],
    },
    {
      title: '.Net Developer',
      project: {
        name: 'Exact ERP System',
      },
      description: [
        'Enhanced variance business software applications by Exact includes Exact Synergy and Exact Globe',
        'Embodied agile software principles throughout the software development life cycle',
        'Designed, coded and tested quality custom solution or functions based on business needs',
        'Maintained and troubleshooted applications by monitoring, identifying and correcting software defects',
      ],
      skills: ['JavaScript', 'HTML', 'CSS', 'JQuery', 'c#', 'ASP', 'vb'],
      technologies: ['.Net', 'Exact Synergy ERP'],
    },
  ];
}
