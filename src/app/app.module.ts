import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoxComponent } from './ui/box/box.component';
import { GridComponent } from './ui/grid/grid.component';
import { NewPageComponent } from './new-page/new-page/new-page.component';

@NgModule({
  declarations: [AppComponent, BoxComponent, GridComponent, NewPageComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
