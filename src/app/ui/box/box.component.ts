import { ChangeDetectionStrategy, Component, Host, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'ui-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxComponent {
  @Input() theme: 'light' | 'dark' = 'light';

  @HostBinding('class.box-container') defaultClass = true;

  @HostBinding('class.is-light') get lightThemeClass() {
    return this.theme === 'light';
  }

  @HostBinding('class.is-dark') get darkThemeClass() {
    return this.theme === 'dark';
  }
}
